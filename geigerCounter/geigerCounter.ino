/******************************************************************************/
/*                                                                            */
/*   Furgalladas Schalter und Sensoren                                        */
/*                                                                            */
/*   This code is released under MIT license                                  */
/*                                                                            */
/******************************************************************************/


/** \file

   geigerCounter.ino - Geiger counter with LCD


   <table border="0" cellspacing="0" cellpadding="0">
   <tr> <td> Source:   </td> <td> geigerCounter.ino          </td></tr>
   <tr> <td> Revision: </td> <td> 1.1                        </td></tr>
   <tr> <td> Status:   </td> <td> ACCEPTED                   </td></tr>
   <tr> <td> Author:   </td> <td> Miguel Carpacho            </td></tr>
   <tr> <td> Date:     </td> <td> 10-OCTOBER-2018 20:00:56   </td></tr>
   </table>

   \n
   <table border="0" cellspacing="0" cellpadding="0">
   <tr> <td> COMPONENT: </td> <td> geigerCounter </td></tr>
   <tr> <td> TARGET:    </td> <td> Arduino </td></tr>
   </table>
*/

/* Note about count per second to microsievert conversion */
/* For tube J305, the conversion is: uSv/h= 0.00812 * CPM.*/

/*MIT License

Copyright (c) [2018] [M.Carpacho]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

/*
* CPM counting algorithm is very simple, it just collect GM Tube events during presettable log period.
* For radiation monitoring station it's recommended to use 30-60 seconds logging period. Feel free to modify
* or add functions to this sketch. This Arduino software is an example only for education purpose without any
* warranty for precision radiation measurements. You are fully responsible for your safety in high
* radiation area!!
* --------------------------------------------------------------------------------------
* WHAT IS CPM?
* CPM (or counts per minute) is events quantity from Geiger Tube you get during one minute. Usually it used to 
* calculate a radiation level. Different GM Tubes has different quantity of CPM for background. Some tubes can produce
* about 10-50 CPM for normal background, other GM Tube models produce 50-100 CPM or 0-5 CPM for same radiation level.
* Please refer your GM Tube datasheet for more information. Just for reference here, SBM-20 can generate 
* about 10-50 CPM for normal background.
* --------------------------------------------------------------------------------------
* HOW TO CONNECT GEIGER KIT?
* The kit 3 wires that should be connected to Arduino UNO board: 5V, GND and INT. PullUp resistor is included on
* kit PCB. Connect INT wire to Digital Pin#2 (INT0), 5V to 5V, GND to GND. Then connect the Arduino with
* USB cable to the computer and upload this sketch. 
*
* Connect button to pin 8
* Also, connect LCD to A4 and A5 pins as follows: 
* SDA => A4
* SCL => A5
*/

//LCD variables (LCD TYPE: 16x2, cell 5x8)

#include <Wire.h>
#include <LCD.h>
#include <LiquidCrystal_I2C.h>  // F Malpartida's NewLiquidCrystal library
//Download: https://bitbucket.org/fmalpartida/new-liquidcrystal/downloads
// Move original LiquidCrystal library elsewhere, copy this in it's place

//

#define I2C_ADDR    0x3f  // Define I2C Address for the PCF8574T 
//---(Following are the PCF8574 pin assignments to LCD connections )----
// This are different than earlier/different I2C LCD displays
#define BACKLIGHT_PIN  3
#define En_pin  2
#define Rw_pin  1
#define Rs_pin  0
#define D4_pin  4
#define D5_pin  5
#define D6_pin  6
#define D7_pin  7

#define  LED_OFF  1
#define  LED_ON  0

LiquidCrystal_I2C  lcd(I2C_ADDR,En_pin,Rw_pin,Rs_pin,D4_pin,D5_pin,D6_pin,D7_pin);

//Own functions 
uint16_t calculateCountValue(uint8_t bufferPosition, uint8_t Mode, uint8_t coeff);

//Custom glyph
byte nuclear[8] = {
  B00000,
  B00000,
  B10001,
  B11011,
  B00000,
  B00100,
  B01110,
  B00000
};


//Custom definitions: 
#define MAX_LENGTH_BUFFER               30
#define ONESECONDMODESEC                1
#define FIVESECONDMODESEC               5
#define TENSECONDMODESEC                10
#define FIFTEENSECONDMODESEC            15
#define THIRTYSECONDMODESEC             30

#define ONESECONDMODE                   0
#define FIVESECONDMODE                  1
#define TENSECONDMODE                   2
#define FIFTEENSECONDMODE               3
#define THIRTYSECONDMODE                4
#define MAX_NUM_MODES                   4


#define FIVESECONDMODECOEFFICIENT       12
#define TENSECONDMODECOEFFICIENT        6
#define FIFTEENSECONDMODECOEFFICIENT    4
#define THIRTYSECONDMODECOEFFICIENT     2


#define LOG_PERIOD     15000  //Logging period in milliseconds, recommended value 15000-60000.
#define MAX_PERIOD     60000  //Maximum logging period without modifying this sketch
#define INIT_TIME      5000   //Init time, in miliseconds
#define BUTTON_PIN     8

//Original variables
unsigned long counts;     //variable for GM Tube events
//Carpa variables
uint8_t countBuffer[30];//Buffer of counting
uint8_t iterator;
unsigned long timevar;
byte state; //State variable
unsigned long previousSecond;
unsigned long currentSecond;
boolean updateLcd;
uint16_t tempCounter;
uint8_t bufferPosition; //Indicates the buffer position of current second
uint8_t auxBufferCalculus;
float milisieverts;

void countParticle(){       //subprocedure for capturing events from Geiger Kit
  counts++;
}

void setup(){
  //Pin definitions
  pinMode(BUTTON_PIN, INPUT);
  counts = 0;
  Serial.begin(9600);
  attachInterrupt(0, countParticle, FALLING); //define external interrupts 

  //Init count buffer
  for (iterator=0;iterator<MAX_LENGTH_BUFFER; iterator++){
    countBuffer[iterator]=0x00;
  }
  state = ONESECONDMODE;
  //Init lcd
   lcd.begin (16,2);  // initialize the lcd 
   lcd.createChar(1, nuclear);
  // Switch on the backlight
   lcd.setBacklightPin(BACKLIGHT_PIN,POSITIVE);
   lcd.setBacklight(LED_ON);
   lcd.clear();
   delay(1000);
   lcd.home();   
   lcd.backlight();  
   lcd.setCursor(0,0);
   lcd.print(" Geiger counter ");
   lcd.setCursor(0,1);
   lcd.print("Initializing...");
   lcd.setCursor(0,0);
   lcd.write(byte(1));
   lcd.setCursor(15,0);
   lcd.write(byte(1));
   timevar=millis();
   previousSecond = round(timevar/1000);
   while (timevar<INIT_TIME){
      //Guess current second
      Serial.print("current second: ");
      Serial.println(timevar);
      timevar=millis();
      currentSecond = round(timevar/1000);
      if (currentSecond>previousSecond){
        countBuffer[previousSecond]=counts;
        counts=0;
        previousSecond=currentSecond;
      }      
   }
   lcd.setCursor(0,0);
   lcd.write("                ");
   lcd.setCursor(0,1);
   lcd.write("                ");
}

void loop(){
  uint16_t valueToPrint;
  //First, update the buffer with count values
  timevar=millis();
  currentSecond = round(timevar/1000);
  bufferPosition=currentSecond%MAX_LENGTH_BUFFER;
  if (currentSecond>previousSecond){
    countBuffer[previousSecond%MAX_LENGTH_BUFFER]=counts;
    counts=0;
    previousSecond=currentSecond;
    updateLcd=true;
  }      

  //See button status
  if (digitalRead(BUTTON_PIN)==HIGH){
    state++;
    if (state==MAX_NUM_MODES+1){
      state=0x00;
    }
    //Reset LCD
    lcd.setCursor(0,0);
    lcd.write("                ");
    lcd.setCursor(0,1);
    lcd.write("                ");
  }
  
  if (updateLcd){
    //Now, see if we need to calculate and update the lcd value
    switch (state){
      case(ONESECONDMODE):
        if (bufferPosition==0){//First position of buffer, so we must print last position
          lcd.setCursor(0,0);
          lcd.print("                ");//Clear previous measurement
          lcd.setCursor(0,0);
          lcd.print("CPS: ");
          lcd.setCursor(6,0);
          lcd.print((uint8_t)countBuffer[MAX_LENGTH_BUFFER-1]);          
        }else{
          lcd.setCursor(0,0);
          lcd.print("                ");//Clear previous measurement
          lcd.setCursor(0,0);
          lcd.print("CPS: ");
          lcd.setCursor(6,0);
          lcd.print((uint8_t)countBuffer[bufferPosition-1]);   
        }
      break;
  
      case(FIVESECONDMODE):
        //Sum the previous five positions of the buffer
        //Call the function
        valueToPrint = calculateCountValue(bufferPosition, FIVESECONDMODESEC,FIVESECONDMODECOEFFICIENT);
        //Clear previous value
        lcd.setCursor(0,0);
        lcd.print("                ");
        lcd.setCursor(0,0);
        lcd.print("CPM(5Sec): ");
        lcd.setCursor(12,0);
        lcd.print(valueToPrint);
        lcd.setCursor(0,1);
        lcd.print("uSv/h: ");
        lcd.print(0.00812*valueToPrint);
      break;
  
      case(TENSECONDMODE):
        //Sum the previous five positions of the buffer
        //Call the function
        valueToPrint = calculateCountValue(bufferPosition, TENSECONDMODESEC,TENSECONDMODECOEFFICIENT);
        lcd.setCursor(0,0);
        lcd.print("CPM(10Sec): ");
        lcd.setCursor(12,0);
        lcd.print(valueToPrint);
        lcd.setCursor(0,1);
        lcd.print("uSv/h: ");
        lcd.print(0.00812*valueToPrint);
      break;
  
      case(FIFTEENSECONDMODE):
        //Sum the previous five positions of the buffer
        //Call the function
        valueToPrint = calculateCountValue(bufferPosition, FIFTEENSECONDMODESEC,FIFTEENSECONDMODECOEFFICIENT);
        //Clear line
        lcd.setCursor(0,0); 
        lcd.print("                ");
        lcd.setCursor(0,0);
        lcd.print("CPM(15Sec): ");
        lcd.setCursor(12,0);
        lcd.print(valueToPrint);
        lcd.setCursor(0,1);
        lcd.print("uSv/h: ");
        lcd.print(0.00812*valueToPrint);
      break;
  
      case(THIRTYSECONDMODE):
        //Sum the previous five positions of the buffer
        Serial.println("THIRTY second mode");
        //Call the function
        valueToPrint = calculateCountValue(bufferPosition, THIRTYSECONDMODESEC,THIRTYSECONDMODECOEFFICIENT);
        lcd.setCursor(0,0); 
        lcd.print("                ");
        lcd.setCursor(0,0);
        lcd.print("CPM(30Sec): ");
        lcd.setCursor(12,0);
        lcd.print(valueToPrint);
        lcd.setCursor(0,1);
        lcd.print("uSv/h: ");
        lcd.print(0.00812*valueToPrint);
      break;
    }
    updateLcd=false;
  }
  delay (100);
  
}

uint16_t calculateCountValue(uint8_t bufferPosition, uint8_t Mode, uint8_t coeff){
  uint16_t tempCounter=0;
  if ((bufferPosition)>=Mode){
    for (iterator=0; iterator<Mode; iterator++){
      tempCounter+=countBuffer[bufferPosition-1-iterator];
    }
  }else{//Buffer overflow
      //From 0 to buffer position
      for (iterator = 0; iterator<bufferPosition; iterator++){
        tempCounter+=countBuffer[bufferPosition-1-iterator];
      }
      for (iterator=(MAX_LENGTH_BUFFER-(Mode-bufferPosition)); iterator<MAX_LENGTH_BUFFER; iterator++){
        tempCounter+=countBuffer[iterator];
      }
  }
  return tempCounter*coeff;
}




